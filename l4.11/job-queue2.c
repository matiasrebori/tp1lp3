#include <malloc.h>
#include <pthread.h>
struct job {
	/* Link field for linked list. */
	struct job* next;
	int id;
	/* Other fields describing work to be done... */
};
/* A linked list of pending jobs. */
struct job* job_queue;
/* A mutex protecting job_queue. */
pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
/* Process queued jobs until the queue is empty. */
void* thread_function ()
{
	while (1) {
		struct job* next_job;
		/* Lock the mutex on the job queue. */
		pthread_mutex_lock (&job_queue_mutex);
		/* Now its safe to check if the queue is empty. */
		if (job_queue == NULL)
			next_job = NULL;
		else {
			/* Carry out the work. */
			//process_job (next_job);
			printf("Working with job %d\n", job_queue->id);
			/* Get the next available job. */
			next_job = job_queue;
			/* Remove this job from the list. */
			job_queue = job_queue->next;
		}
		/* Unlock the mutex on the job queue because were done with the
		queue for now. */
		pthread_mutex_unlock (&job_queue_mutex);
		/* Was the queue empty? If so, end the thread. */
		if (next_job == NULL)
			break;
		
		/* Clean up. */
		free (next_job);
	}
	return NULL;
}

int main(){
	struct job* job1 = malloc(sizeof(job_queue));
	struct job* job2 = malloc(sizeof(job_queue));
	
	job1->id = 1;
	job2->id = 2;
	
	job_queue = malloc(sizeof(job_queue));
	job_queue->id = 0;
	job_queue->next = job1;
	job1->next = job2;
	job2->next = NULL;
	thread_function();
	return 0;
}

