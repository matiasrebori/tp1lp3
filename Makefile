VPATH = /l2.1
##############
# Listing's 1 #
##############

#listing 1.1,1.2,1.3
reciprocal: l1.1/main.o l1.2/reciprocal.o
	g++ $(CFLAGS) -o reciprocal l1.1/main.o l1.2/reciprocal.o
main.o: l1.1/main.c l1.3/reciprocal.hpp
	gcc $(CFLAGS) -c l1.1/main.c
reciprocal.o: l1.2/reciprocal.cpp l1.3/reciprocal.hpp
	g++ $(CFLAGS) -c l1.2/reciprocal.cpp
clean_reciprocal:
	rm -f l1.1/*.o l1.2/*.o reciprocal

##############
# Listing's 2 #
##############

#listing 2.1
arglist: arglist.o
	cc $(CFLAGS) -o arglist arglist.o
arglist.o:
	cc $(CFLAGS) -c arglist.c
clean_arglist:
	rm -f arglist

#listing 2.2
getopt_long: l2.2/getopt_long.c
	gcc $(CFLAGS) l2.2/getopt_long.c -o getopt_long
clean_getopt_long:
	rm -f getopt_long

