#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
struct job {
	/* Link field for linked list. */
	int id;
	struct job* next;
	/* Other fields describing work to be done... */
};
/* A linked list of pending jobs. */
struct job* job_queue;
/* Process queued jobs until the queue is empty. */
void* thread_function ()
{
	while (job_queue != NULL) {
		/* Get the next available job. */
		struct job* next_job = job_queue;
		/* Carry out the work. */
		//process_job (next_job);
		printf("Working with job %d\n", job_queue->id);
		/* Remove this job from the list. */
		job_queue = job_queue->next;
		/* Clean up. */
		free (next_job);
	}
	return NULL;
}

int main(){
	struct job* job1 = malloc(sizeof(job_queue));
	struct job* job2 = malloc(sizeof(job_queue));
	
	job1->id = 1;
	job2->id = 2;
	
	job_queue = malloc(sizeof(job_queue));
	job_queue->id = 0;
	job_queue->next = job1;
	job1->next = job2;
	job2->next = NULL;
	thread_function();
	return 0;
}

